import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import App from '../src/App';
import Header from '../src/Header';
import Footer from '../src/Footer';
import AddTodo from '../src/AddTodo';
import RemoveTodo from '../src/RemoveTodo';
import TodoList from '../src/TodoList';


describe("Frontend Testing", function() {
    describe("App Testing", function() {
        it ("Component Rendering", function() {
            const wrapper = shallow(<App />)
            
            expect(wrapper).to.exist
        })

        describe("Add Function Test", function() {

            // Check if objects have been added and order has been preserved
            function checkOrder(before, items, after) {
                let items_index = 0;
                let after_index = 0;

                for(; items_index < items.length; items_index++) {
                    if (items[items_index] != null && items[items_index] != "") {
                        expect(items[items_index]).to.equal(after[after_index + before.length].task)
                        after_index++
                    }
                }
                
                expect(before.length).to.equal(after.length - after_index)

                for (let i = 0; i < before.length; i++) {
                    expect(after[i].task).to.equal(before[i].task)
                    expect(after[i].id).to.equal(before[i].id)
                }
            }

            // Check if item keys are unique
            function checkKeys(list) {
                for (let i = 0; i < list.length; i++) {
                    for (let j = i + 1; j < list.length; j++) {
                        expect(list[i].key).to.not.equal(list[j].id)
                    }
                }
            }

            it ("Add Function: Null/Empty String", function() {
                const wrapper = shallow(<App />)
                const before = wrapper.state('items')
                let item_null = null
                let item_empty = ""

                // Check adding null and empty string to list
                wrapper.instance().add(item_null)
                checkOrder(before, [item_null], wrapper.state('items'))
                checkKeys(wrapper.state('items'))

                wrapper.instance().add(item_empty)
                checkOrder(before, [item_empty], wrapper.state('items'))
                checkKeys(wrapper.state('items'))
            })

            it ("Add Function: Empty List", function() {
                const wrapper = shallow(<App />)
                const before = wrapper.state('items')
                const item = 'Bread'

                // Check adding item to empty list
                wrapper.instance().add(item)
                checkOrder(before, [item], wrapper.state('items'))
                checkKeys(wrapper.state('items'))
            })

            it ("Add Function: Non-empty List", function() {
                const wrapper = shallow(<App />)
                wrapper.setState({ 
                    items: [
                        {todo: "Bread", id: 1},
                        {todo: "Milk", id: 2},
                        {todo: "Salty Snacks", id: 3},
                        {todo: "Frozen Dinners", id: 4},
                        {todo: "Cereal", id: 5},
                        {todo: "Beer", id: 6},
                        {todo: "Soda", id: 7}
                    ],
                    count: 8
                })
                const before = wrapper.state('items')
                const item = 'Ice Cream'

                // Check adding item to non-empty list
                wrapper.instance().add(item)
                checkOrder(before, [item], wrapper.state('items'))
                checkKeys(wrapper.state('items'))
            })

            it ("Add Function: Multiple Adds", function() {
                const wrapper = shallow(<App />)
                const before = wrapper.state('items')
                const items = [
                    "Soda",
                    "Beer",
                    "Cereal",
                    "Frozen Dinners",
                    "Salty Snacks",
                    "Milk",
                    "Bread"    
                ]

                // Check adding multiple items
                for (let i = 0; i < items.length; i++) {
                    wrapper.instance().add(items[i])
                }
                checkOrder(before, items, wrapper.state('items'))
                checkKeys(wrapper.state('items'))
            })
        })

        describe ("Remove Function Testing", function() {

            const wrapper = shallow(<App />)

            // Check if object has been removed and order has been preserved
            function checkOrder(before, index, after) {
                expect(after.length).to.equal(before.length - 1)

                let before_index = 0;
                let after_index = 0;

                for(;after_index < after.length; after_index++, before_index++) {
                    if (before_index === index) {
                        before_index++
                    }
                    expect(after[after_index].task).to.equal(before[before_index].task)
                    expect(after[after_index].id).to.equal(before[before_index].id)
                }
            }

            // Get list state before and after remove function
            function getStates(wrapper, index) {
                let before_items = wrapper.state('items')
                wrapper.instance().remove(before_items[index].id)
                let after_items = wrapper.state('items')

                return [before_items, after_items]
            }

            beforeEach(function() {
                wrapper.setState({ 
                    items: [
                        {todo: "Bread", id: 1},
                        {todo: "Milk", id: 2},
                        {todo: "Salty Snacks", id: 3},
                        {todo: "Frozen Dinners", id: 4},
                        {todo: "Cereal", id: 5},
                        {todo: "Beer", id: 6},
                        {todo: "Soda", id: 7}
                    ],
                    count: 8
                })
            })

            it ("Remove: Front of List", function() {
                let [before, after] = getStates(wrapper, 0)
                checkOrder(before, 0, after)
            })

            it ("Remove: End of List", function() {
                let [before, after] = getStates(wrapper, wrapper.state('items').length - 1)
                checkOrder(before, before.length - 1, after)
            })

            it ("Remove: Middle of List", function() {
                let [before, after] = getStates(wrapper, Math.floor(wrapper.state('items').length / 2))
                checkOrder(before, Math.floor(before.length / 2), after)
            })
        })        
    })

    describe ("Header Testing", function() {
        
        it ("Component Rendering", function() {
            const wrapper = shallow(<Header />)
            
            expect(wrapper).to.exist
        })
    })

    describe ("Footer Testing", function() {
        
        it ("Component Rendering", function() {
            const wrapper = shallow(<Footer />)
            
            expect(wrapper).to.exist
        })
    })

    describe ("AddTodo Testing", function() {

        it ("Component Rendering", function() {
            const wrapper = shallow(<AddTodo items = {[]} add = {function() {}}/>)
            
            expect(wrapper).to.exist
        })
    })

    describe ("RemoveTodo Testing", function() {

        it ("Component Rendering", function() {
            const wrapper = shallow(<RemoveTodo items = {[]} remove = {function() {}}/>)
            
            expect(wrapper).to.exist
        })
    })

    describe ("TodoList Testing", function() {
        
        it ("Component Rendering", function() {
            const wrapper = shallow(<TodoList items = {[]} remove = {function() {}}/>)
            
            expect(wrapper).to.exist
        })
    })
})