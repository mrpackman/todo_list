import React from 'react';

function Footer() {

    return (
        <footer className="page-footer teal">
            <div className="container">
                Alec Hillyard © 2020 Copyright
                <i className="material-icons right">email</i>
                <i className="material-icons right">textsms</i>
                <i className="material-icons right">call</i>
            </div>
            <br></br>
        </footer>
    )

}

export default Footer