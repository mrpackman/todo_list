import React, { Component } from 'react';
import TodoList from './TodoList';
import AddTodo from './AddTodo';
import RemoveTodo from './RemoveTodo';
import Header from './Header';
import Footer from './Footer';

import './App.css';

class App extends Component {

  state = {
    items: [],
    count: 1
  }

  add = (todo) => {
    if (todo == null || todo.length < 1) {
      return;
    }

    const new_todo = {
      task: todo,
      id : this.state.count
    }

    this.setState({
      items: [...this.state.items, new_todo],
      count: this.state.count + 1
    })
  }

  remove = (id) => {

    const new_todo_list = this.state.items.filter(todo => {
      return todo.id !== id
    })

    this.setState({items: new_todo_list, count: this.state.count})
  }
  

  render() {
    return (   
      <div className="flexbox">
        <Header />
        <br></br>
        <main className = "container">
          <TodoList items = {this.state.items} remove = {this.remove} />
          <AddTodo items = {this.state.items} add = {this.add} />
          <RemoveTodo items = {this.state.items} count = {this.state.count} remove = {this.remove} />
        </main>
        <br></br>
        <Footer/>
      </div>
    );
  }
}

export default App;
