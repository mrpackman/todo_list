import React from 'react';

function Header() {
    
    return (
        <header>
        <nav>
            <div className="nav-wrapper teal">
                <a href="http://localhost:3000/" className="brand-logo center">
                    TODO LIST
                    <i className="material-icons">format_list_bulleted</i>
                </a>
            </div>
        </nav>
        </header>
    )

}

export default Header