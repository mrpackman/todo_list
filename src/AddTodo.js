import React, {useState} from 'react';

function AddTodo({items, add}) {

    const [task, setTask] = useState('')

    const handleChange = (event) => {
        setTask(event.target.value)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        add(task)
        setTask('')
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>What needs to be done?</label> 
                <input type = 'text' 
                    onChange = {handleChange} 
                    value = {task}
                    placeholder = "Type Here" 
                />
                <button className = "waves-effect waves-light btn-large left">
                    <i className="material-icons left">add</i>
                    ADD #{items.length + 1}
                </button>
            </form>
        </div>
    )
}

export default AddTodo