import React from 'react';

function RemoveTodo({items,remove}) {

    const handleSubmit = (event) => {
        event.preventDefault()
        if (items.length - 1 >= 0) {
            remove(items[items.length - 1].id)
        }
    }

    return (
        <div>
            <form onSubmit = {handleSubmit}>
                <button className = "waves-effect waves-light btn-large right">
                    <i className="material-icons left">remove</i>
                    REMOVE #{items.length}
                </button>
            </form>
        </div>
    )
}

export default RemoveTodo