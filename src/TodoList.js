import React from 'react';

function TodoList({items, remove}) {

    const todoList = items.length ? (
        items.map( todo => {
            return (
                <tr className = "collection-item" key = {todo.id} onClick = {() => remove(todo.id)}>
                    <td>
                        {todo.task}
                    </td>
                </tr>
            )
        })
    ) : (
        <tr>
            <td className = "center">
                Nothing left to do. Good Job!
            </td>
        </tr>
    )

    return (
        <div className = "todos collection">
            <table className = "highlight striped centered">
                <thead>
                    <tr>
                        <th>Task</th>
                    </tr>
                </thead>
                <tbody>
                    {todoList}
                </tbody>
            </table>
        </div>
    )
}

export default TodoList